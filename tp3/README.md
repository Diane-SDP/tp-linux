# TP3

## I. Service SSH

### 1. Analyse du service

🌞 **S'assurer que le service sshd est démarré**

```bash
[diane@localhost ~]$ systemctl status sshd
● sshd.service - OpenSSH server daemon
     Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; preset:>
     Active: active (running) since Mon 2024-01-29 12:14:05 CET; 5min ago
```

🌞 **Analyser les processus liés au service SSH**

```bash
[diane@localhost ~]$ ps -ef | grep sshd
root         692       1  0 12:14 ?        00:00:00 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
root        1294     692  0 12:19 ?        00:00:00 sshd: diane [priv]
diane       1311    1294  0 12:19 ?        00:00:00 sshd: diane@pts/0
```

🌞 **Consulter les logs du service SSH**

```bash
[diane@localhost ~]$  journalctl -u sshd
Jan 29 13:32:06 localhost systemd[1]: Starting OpenSSH server daemon...
Jan 29 13:32:06 localhost sshd[692]: main: sshd: ssh-rsa algorithm is disab>
Jan 29 13:32:06 localhost sshd[692]: Server listening on 0.0.0.0 port 22.
Jan 29 13:32:06 localhost sshd[692]: Server listening on :: port 22.
Jan 29 13:32:06 localhost systemd[1]: Started OpenSSH server daemon.
Jan 29 13:32:18 localhost.localdomain sshd[1282]: main: sshd: ssh-rsa algor>
Jan 29 13:32:20 localhost.localdomain sshd[1282]: Accepted password for dia>
Jan 29 13:32:20 localhost.localdomain sshd[1282]: pam_unix(sshd:session): s>
lines 1-8/8 (END)
[diane@localhost ~]$ sudo tail /var/log/secure -n 2
[sudo] password for diane:
Jan 29 14:30:25 localhost sshd[1510]: Accepted password for diane from 10.2.1.1 port 53379 ssh2
Jan 29 14:30:25 localhost sshd[1510]: pam_unix(sshd:session): session opened for user diane(uid=1000) by (uid=0)
```

## 2. Modification du service

🌞 **Identifier le fichier de configuration du serveur SSH**

```bash
/etc/ssh/sshd_config
```

🌞 **Modifier le fichier de conf**

```bash
[diane@localhost ~]$ echo $RANDOM
31425
[diane@localhost ~]$ sudo nano /etc/ssh/sshd_config
[diane@localhost ~]$ sudo cat /etc/ssh/sshd_config | grep Port
Port 31425
[diane@localhost ~]$ sudo firewall-cmd --remove-port=22/tcp --permanent
[diane@localhost ~]$ sudo firewall-cmd --add-port=31425/tcp --permanent
[diane@localhost ~]$ sudo firewall-cmd --list-all | grep ports
  ports: 31425/tcp
```

🌞 **Redémarrer le service**

```bash
[diane@localhost ~]$ sudo systemctl restart sshd
```

🌞 **Effectuer une connexion SSH sur le nouveau port**

```bash
PS C:\Users\Diane> ssh diane@10.2.1.2 -p 31425
diane@10.2.1.2's password:
Last login: Mon Jan 29 13:32:20 2024 from 10.2.1.1
[diane@localhost ~]$
```

## II. Service HTTP

### 1. Mise en place

🌞 **Installer le serveur NGINX**

```bash
[diane@localhost ~]$ sudo dnf install nginx -y
```

🌞 **Démarrer le service NGINX**

```bash
[diane@localhost ~]$ sudo systemctl start nginx
```

🌞 **Déterminer sur quel port tourne NGINX**

```bash
[diane@localhost ~]$ sudo ss -alnpt | grep nginx
LISTEN 0      511          0.0.0.0:80         0.0.0.0:*    users:(("nginx",pid=1431,fd=6),("nginx",pid=1430,fd=6))
LISTEN 0      511             [::]:80            [::]:*    users:(("nginx",pid=1431,fd=7),("nginx",pid=1430,fd=7))
[diane@localhost ~]$ sudo firewall-cmd --permanent --add-port=80/tcp
success
[diane@localhost ~]$ sudo firewall-cmd --reload
success
```

🌞 **Déterminer les processus liés au service NGINX**

```bash
[diane@localhost ~]$ ps -ef | grep nginx
root        1430       1  0 14:24 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       1431    1430  0 14:24 ?        00:00:00 nginx: worker process
```

🌞 **Déterminer le nom de l'🌟utilisateur🌟 qui lance NGINX**

```bash
[diane@localhost ~]$ ps -ef | grep nginx
root        1430       1  0 14:24 ?        00:00:00 nginx: master process /usr/sbin/nginx
🌟nginx🌟       1431    1430  0 14:24 ?        00:00:00 nginx: worker process
[diane@localhost ~]$ sudo cat /etc/passwd | grep nginx
nginx:x:991:991:Nginx web server:/var/lib/nginx:/sbin/nologin
```

🌞 **Test !**

```bash
$ curl http://10.2.1.2:80 | head -n 7
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  7620  100  7620    0     0  5160k      0 --:--:-- --:--:-- --:--:-- 7441k
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
```

### 2. Analyser la conf de NGINX

🌞 Déterminer le path du fichier de configuration de NGINX

```bash
[diane@localhost ~]$ ls -al /etc/nginx/nginx.conf
-rw-r--r--. 1 root root 2334 Oct 16 20:00 /etc/nginx/nginx.conf
```

🌞 Trouver dans le fichier de conf

```bash
# les lignes qui permettent de faire tourner un site web d'accueil
[diane@localhost ~]$ cat /etc/nginx/nginx.conf | grep server -A 10
    server {
        listen       80;
        listen       [::]:80;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
    }

# Lignes avec include
[diane@localhost ~]$ cat /etc/nginx/nginx.conf | grep include
include /usr/share/nginx/modules/*.conf;
    include             /etc/nginx/mime.types;
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    include /etc/nginx/conf.d/*.conf;
        include /etc/nginx/default.d/*.conf;
#        include /etc/nginx/default.d/*.conf;
```

### 3. Déployer un nouveau site web

🌞 **Créer un site web**

```bash
[diane@localhost ~]$ sudo mkdir /var/www
[diane@localhost ~]$ sudo mkdir /var/www/tp3_linux
[diane@localhost ~]$ sudo nano /var/www/tp3_linux/index.html
# Dans le fichier
<h1>MEOW mon premier serveur web</h1>
```

🌞 **Gérer les permissions**

```bash
[diane@localhost ~]$ sudo chown -R nginx:nginx /var/www/tp3_linux
```

🌞 **Adapter la conf NGINX**

```bash
[diane@localhost ~]$ sudo nano /etc/nginx/nginx.conf
# Suppr le bloc server meow
[diane@localhost ~]$ echo $RANDOM
27076
[diane@localhost ~]$ sudo nano /etc/nginx/conf.d/truc.conf
[diane@localhost ~]$ sudo firewall-cmd --add-port=27076/tcp --permanent
success
```

🌞 **Visitez votre super site web**

```bash
[diane@localhost ~]$ curl http://10.2.1.2:27076
<h1>MEOW mon premier serveur web</h1>
```

## III. Your own services

### 2. Analyse des services existants

🌞 **Afficher le fichier de service SSH**

```bash
[diane@localhost ~]$ sudo systemctl status sshd
● sshd.service - OpenSSH server daemon
     Loaded: loaded (🌟/usr/lib/systemd/system/sshd.service🌟; enabled; preset:>
[diane@localhost ~]$ sudo cat /usr/lib/systemd/system/sshd.service | grep ExecStart=
ExecStart=/usr/sbin/sshd -D $OPTIONS
```

🌞 **Afficher le fichier de service NGINX**

```bash
[diane@localhost ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (🌟/usr/lib/systemd/system/nginx.service🌟; enabled; preset>
[diane@localhost ~]$ sudo cat /usr/lib/systemd/system/nginx.service | grep ExecStart=
ExecStart=/usr/sbin/nginx
```

### 3. Création de service

🌞 **Créez le fichier /etc/systemd/system/tp3_nc.service**

```bash
[diane@localhost ~]$ echo $RANDOM
23358
[diane@localhost ~]$ sudo cat /etc/systemd/system/tp3_nc.service
[Unit]
Description=Super netcat tout fou
[Service]
ExecStart=/usr/bin/nc -l 23358 -k
[diane@localhost ~]$ sudo firewall-cmd --add-port=23358/tcp --permanent
success
[diane@localhost ~]$ sudo firewall-cmd --reload
success
```

🌞 Indiquer au système qu'on a modifié les fichiers de service

```bash
[diane@localhost ~]$ sudo systemctl daemon-reload
```

🌞 Démarrer notre service de ouf

```bash
[diane@localhost ~]$ sudo systemctl start tp3_nc
```

🌞 Vérifier que ça fonctionne

```bash
[diane@localhost ~]$ sudo systemctl status tp3_nc
● tp3_nc.service - Super netcat tout fou
     Loaded: loaded (/etc/systemd/system/tp3_nc.service; static)
     Active: active (running) since Mon 2024-01-29 16:32:21 CET; 34s ago
[diane@localhost ~]$ ss -laputen | grep tp3
tcp   LISTEN 0      10              0.0.0.0:23358      0.0.0.0:*     ino:28668 sk:4a cgroup:/system.slice/tp3_nc.service <->
tcp   LISTEN 0      10                 [::]:23358         [::]:*     ino:28667 sk:4b cgroup:/system.slice/tp3_nc.service v6only:1 <->

# Sur une autre VM
[diane@meow ~]$ nc 10.2.1.2 23358
coucou
meow
 
```

🌞 **Les logs de votre service**

```bash
[diane@localhost ~]$ sudo journalctl -xe -u tp3_nc | grep Started
Jan 29 16:32:21 localhost.localdomain systemd[1]: Started Super netcat tout fou.

[diane@localhost ~]$ sudo journalctl -xe -u tp3_nc | grep nc
Jan 29 16:41:20 localhost.localdomain nc[1941]: coucou
Jan 29 16:42:03 localhost.localdomain nc[1941]: meow
```

🌞 **S'amuser à kill le processus**

```bash
[diane@localhost ~]$ ps -ef | grep nc
dbus         677       1  0 14:20 ?        00:00:00 /usr/bin/dbus-broker-lanch --scope system --audit
root        🌟2062🌟       1  0 16:53 ?        00:00:00 /usr/bin/nc -l 23358 -k
[diane@localhost ~]$ sudo kill 2062
```

🌞 **Affiner la définition du service**

```bash
[diane@localhost ~]$ sudo nano /etc/systemd/system/tp3_nc.service
# Ajout de Restart=always
[diane@localhost ~]$ sudo systemctl daemon-reload
```
