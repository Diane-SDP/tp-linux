# TP 1

## 🌞 Supprimer des fichiers

```bash
[root@localhost boot]$ rm vmlinuz-5.14.0-284.11.1.el9_2.x86_64
```

## 🌞 Mots de passe

```bash
[root@localhost diane]$ nano /etc/shadow
# Et là j'ai remplacé mon hash et celui de root par un MEOW
```

## 🌞 Another way ?

```bash
[root@localhost diane]# chmod a-x /bin/bash
```

## 🌞 Effacer le contenu du disque dur

```bash
[root@localhost ~]$ dd if=/dev/zero of=/dev/mapper/rl-root status=progress
```

## 🌞 Reboot automatique

J'ai créé un script qui reboot, et j'ai fait un nouveau service qui lance ce script au démarrage

```bash
[diane@localhost ~]$ nano script.sh
# DANS LE FICHIER
#!/bin/bash
reboot
[diane@localhost ~]$ chmod +x script.sh
[diane@localhost ~]$ nano /etc/systemd/system/reboot.service
# DANS LE FICHIER
[Unit]
Description=reboot meow

[Service]
ExecStart=/home/diane/script.sh

[Install]
WantedBy=default.target
[diane@localhost ~]$ sudo systemctl daemon-reload
[diane@localhost ~]$ sudo systemctl enable reboot.service
[diane@localhost ~]$ sudo systemctl start reboot.service
```

## 🌞 Trouvez 4 autres façons de détuire la machine

- Delete bash

```bash
[root@localhost bin]# rm bash
```

- MEOW

```bash
[diane@localhost ~]$ nano uwu.sh
# DANS LE FICHIER
#!/bin/bash
trap '' INT
while true
do
    echo "meow"
done
[diane@localhost ~]$ chmod +x uwu.sh
[diane@localhost ~]$ echo "./uwu.sh" >> /etc/profile
```

- PWD

```bash
[root@localhost diane]# nano /etc/passwd
root:x:0:0:root:/root:/bin/meow #meow a la place de bash
diane:x:1000:1000:diane:/home/diane:/bin/meow #la aussi
```
