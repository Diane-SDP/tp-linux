# TP5

## Partie 1 : Script carte d'identité

```bash
[diane@localhost idcard]$ ./idcard.sh
Machine name :
0S Rocky Linux and kernel version is 5.14.0-284.11.1.el9_2.x86_64
RAM : 415M memory available on 771M total memory
Disk : 4.6G space left
Top 5 processes by RAM usage :
- root         679  0.0  5.2 127724 41336 ?        Ssl  13:46   0:00 /usr/bin/python3 -s /usr/sbin/firewalld --nofork --nopid
- root         694  0.0  2.9 257248 23456 ?        Ssl  13:46   0:00 /usr/sbin/NetworkManager --no-daemon
- root           1  0.0  2.0 171768 15936 ?        Ss   13:46   0:00 /usr/lib/systemd/systemd --switched-root --system --deserialize 31
- diane       1302  0.0  1.7  22228 13592 ?        Ss   13:46   0:00 /usr/lib/systemd/systemd --user
- root         583  0.0  1.5  34352 12428 ?        Ss   13:46   0:00 /usr/lib/systemd/systemd-udevd
Listening ports :
      - 323 udp : chronyd
      -   : chronyd
      - 22 tcp : sshd
      -   : sshd
  - /home/diane/.local/bin
  - /home/diane/bin
  - /usr/local/bin
  - /usr/bin
  - /usr/local/sbin
  - /usr/sbin
Here is your random cat (jpg file) : https://www.youtube.com/watch?v=hvL1339luv0
```

## Partie 2 : Script youtube

```bash
# PREMIER SCRIPT
[diane@localhost yt]$ ./yt.sh "https://www.youtube.com/watch?v=Z_2wU0_fNuM"
Video https://www.youtube.com/watch?v=Z_2wU0_fNuM was downloaded.
File path : /srv/yt/downloads/PEPPA PUTE/PEPPA PUTE.mp4


# SECOND SCRIPT ET SERVICE
[diane@localhost ~]$ sudo systemctl status yt
[diane@localhost ~]$ systemctl status yt 
● yt.service - downloads videos from url in file toDownloads
     Loaded: loaded (/etc/systemd/system/yt.service; enabled; preset: disab>
     Active: active (running) since Tue 2024-03-05 16:11:59 CET; 27s ago
   Main PID: 695 (yt-v2.sh)
      Tasks: 2 (limit: 4591)
     Memory: 1.0M
        CPU: 18ms
     CGroup: /system.slice/yt.service
             ├─ 695 /bin/bash /srv/yt/yt-v2.sh
             └─4146 sleep 10
[diane@localhost ~]$ journalctl -xe -u yt   
Mar 05 16:12:30 localhost systemd[1]: Started downloads videos from url in >
░░ Subject: A start job for unit yt.service has finished successfully
░░ Defined-By: systemd
░░ Support: https://wiki.rockylinux.org/rocky/support
░░

░░ A start job for unit yt.service has finished successfully.
░░
░░ The job identifier is 251.   
Mar 05 16:12:41 localhost.localdomain yt-v2.sh[4154]: Video https://www.youtube.com/watch?v=sNx57atloH8 was downloaded.
Mar 05 16:12:41 localhost.localdomain yt-v2.sh[4155]: File path : /srv/yt/downloads/tomato anxiety/tomato anxiety.mp4
Mar 05 16:12:51 localhost.localdomain yt-v2.sh[4159]: Video https://www.youtube.com/watch?v=Z_2wU0_fNuM was downloaded.
Mar 05 16:12:51 localhost.localdomain yt-v2.sh[4160]: File path : /srv/yt/downloads/PEPPA PUTE/PEPPA PUTE.mp4
```
